# Gradle 7.2 全量包下载

## 简介

本仓库提供了一个资源文件 `gradle-7.2-all.zip` 的下载链接，该文件是 Android 开发工具 Android Studio 编译项目时必备的 Gradle 工具包。由于在 Android Studio 中直接下载 Gradle 的速度通常非常慢，导致项目编译受阻，因此我们将下载好的 Gradle 7.2 全量包分享出来，方便开发者快速获取并使用。

## 文件描述

- **文件名**: `gradle-7.2-all.zip`
- **文件大小**: [文件大小]
- **MD5**: [MD5 校验值]
- **SHA-1**: [SHA-1 校验值]

## 使用方法

1. **下载文件**: 点击 [这里](链接地址) 下载 `gradle-7.2-all.zip` 文件。
2. **解压文件**: 将下载的 `gradle-7.2-all.zip` 文件解压到你的 Android Studio 项目所需的 Gradle 目录中。
3. **配置项目**: 在 Android Studio 中配置项目的 `gradle-wrapper.properties` 文件，确保其指向正确的 Gradle 版本。
4. **编译项目**: 重新编译你的 Android 项目，Gradle 工具包将正常工作。

## 注意事项

- 请确保下载的文件完整且未被篡改，建议使用提供的 MD5 或 SHA-1 校验值进行验证。
- 如果你在其他项目中使用不同的 Gradle 版本，请根据需要下载相应的版本。

## 贡献

如果你有其他版本的 Gradle 资源包，欢迎提交 Pull Request 或 Issue，帮助更多的开发者解决 Gradle 下载慢的问题。

## 许可证

本仓库中的资源文件遵循 [Apache License 2.0](LICENSE) 开源协议。